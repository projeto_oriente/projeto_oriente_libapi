from os.path import join

import requests


class DoesNotExist(Exception):
    pass


class MultipleObjectsReturned(Exception):
    pass


class ApiManager(object):

    def __init__(self, user=None, password=None, endpoint=None):
        self.session = requests.Session()
        self.endpoint = "https://oriente.pythonanywhere.com/api/"

        if endpoint is not None:
            self.endpoint = join(endpoint, "")

        if user is not None:
            assert isinstance(user, str), "Precisar sem uma str no user"
            assert isinstance(password, str), "Precisar sem uma str no password"
            self.session.auth = requests.auth.HTTPBasicAuth(user, password)

    def get(self, endpoint, *args, **kwargs):
        return self.session.get(join(self.endpoint, endpoint, ""), *args, **kwargs)

    def global_get(self, endpoint, *args, **kwargs):
        return self.session.get(endpoint, *args, **kwargs)

    def post(self, endpoint, *args, **kwargs):
        return self.session.post(join(self.endpoint, endpoint, ""), *args, **kwargs)


class OrienteWebApi(ApiManager):

    def get_documentos(self, id=None, url=None):
        endpoint = join(self.endpoint, "documentos")
        data = {}

        if id is not None:
            endpoint = join(endpoint, str(id))
        endpoint = join(endpoint, "")

        if id is None and url is not None:
            data["url"] = url

        response = self.session.get(endpoint, data=data)
        return response

    def post_documentos(self, data):
        endpoint = join(self.endpoint, "documentos")
        endpoint = join(endpoint, "")
        response = self.session.post(endpoint, data=data)

        return response

    def post_documento(self, data):
        endpoint = join(self.endpoint, "documentos")
        endpoint = join(endpoint, "")
        response = self.session.post(endpoint, data=data)

        return response

    def put_documentos(self):
        pass


class ResultIter(object):

    def __len__(self):
        return self.count

    def __init__(self, model, **kwargs):
        self.next_page = kwargs.get("next", None)
        self.count = kwargs.get("count", 0)
        self.previous_page = kwargs.get("previous")
        self.result_iter = iter(kwargs.get("results"))
        self.model = model

    def __iter__(self):
        return self

    def json_to_model(self, json: dict):
        return self.model(json)

    def get_next_object(self):
        json_response = next(self.result_iter)
        return self.json_to_model(json_response)

    def __next__(self):
        try:
            return self.get_next_object()
        except StopIteration:
            if self.next_page is None:
                raise StopIteration
            else:
                r = self.model.get_next(self.next_page)
                rr = ResultIter(self.model, **r)
                self.next_page = rr.next_page
                self.previous_page = rr.previous_page
                self.result_iter = rr.result_iter
                return self.get_next_object()


class Instance(object):
    api_manager = None
    endpoint = None
    campos = {}
    query_filter = None

    def __str__(self):
        return str(self.data)

    def __init__(self, data={}):
        self.data = data
        self.update_endpoint_id()

    def update_endpoint_id(self):
        self.endpoint_id = self.get_endpoint_id()

    def get_endpoint_id(self):
        raise Exception("configurado errado, implemente set_endpoint_id")

    @classmethod
    def get(cls, endpoint, **kwargs):

        if cls.query_filter is not None:
            endpoint_query = f"{endpoint}?{cls.query_filter}"
            cls.query_filter = None
        else:
            endpoint_query = endpoint

        response = cls.api_manager.get(endpoint_query, **kwargs)
        if response.status_code == 404:
            raise DoesNotExist

        if response.status_code == 200:
            return response.json()
        else:
            msg = "{} - {}".format(response.status_code, response.text)
            raise Exception(msg)

    @classmethod
    def get_next(cls, endpoint):
        return cls.api_manager.global_get(endpoint).json()

    @classmethod
    def get_by_id(cls, id):
        json = cls.get(join(cls.endpoint, str(id)))
        return cls(data=json)

    @classmethod
    def filter(cls, **kwargs):
        query = ""
        for k in kwargs:
            query += f"{k}={kwargs[k]}&"
        cls.query_filter = query
        return cls

    @classmethod
    def get_all(cls):
        response = cls.get(cls.endpoint)
        return ResultIter(cls, **response)

    @classmethod
    def get_filter(cls, data={}):
        json = cls.get(cls.endpoint, data=data)

        count = json.get("count", 0)
        if count > 1:
            raise MultipleObjectsReturned("mais de um objeto restornado")
        elif count < 1:
            raise DoesNotExist("objeto não existe")
        else:
            return cls(data=json[0])

    def is_valid(self):
        for campo in self.campos:
            if campo not in self.data:
                if self.campos[campo]:
                    return not self.campos[campo]
        return True

    def create(self):
        response = self.api_manager.post(self.endpoint, data=self.data)
        if response.status_code in (200, 201):
            json = response.json()
            self.data = json
            self.update_endpoint_id()
        else:
            msg = "{} - {}".format(response.status_code, response.text)
            raise Exception(msg)

    def update(self):
        pass

    def save(self):
        if self.is_valid():
            if self.endpoint_id is None:
                return self.create()
            else:
                return self.update()
        else:
            raise Exception("invalid Instance")

    def key(self, k):
        return self.data.get(k, None)


class Documento(Instance):
    endpoint = "documentos"
    campos = {"id": False, "url": True, "url_arquivo": True, "titulo": True}

    def get_endpoint_id(self):
        if "id" in self.data:
            return join(self.endpoint, str(self.data["id"]), "")
        else:
            return None


class Recomendacao(Instance):
    endpoint = "recomendacoes"
    campos = {"id": False, "documento_origem": True, "documento_destino": True, "fator": True}

    def get_endpoint_id(self):
        if "id" in self.data:
            return join(self.endpoint, str(self.data["id"]), "")
        else:
            return None

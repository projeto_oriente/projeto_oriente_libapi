import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

install_dependencies = [
    'requests==2.21.0',
]

setuptools.setup(
    name="oriente_libapi",
    version="0.0.1",
    author="Paulo Roberto Cruz",
    author_email="paulo.cruz9@gmail.com",
    description="Api Rest para comunicar com Oriente Web",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/projeto_oriente/projeto_oriente_libapi",
    packages=setuptools.find_packages(),
    install_requires = install_dependencies,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
from oriente_libapi import Documento, ApiManager

# sem credenciais

api_manager = ApiManager(endpoint="https://oriente.pythonanywhere.com/api/")
Documento.api_manager = api_manager

docs = Documento.get_all()

print(len(docs))
for doc in docs:
    print("doc: ",doc)

dc = Documento.get_by_id(700)

for d in dc.data:
    print(d, ": ",  dc.data[d])

"""
# com credenciais
api_manager = ApiManager(user = "admin", password="1234567o", endpoint="https://oriente.pythonanywhere.com/api/")
Documento.api_manager = api_manager
docs = Documento.get_all()

for doc in docs:
    print(doc)

documento = Documento(data={"url":"http://asdf.com", "url_arquivo":"http://asdfasdfasdf.com"})
documento.save()
"""
